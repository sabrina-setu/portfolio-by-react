const Contacts = () => {
  return(
<section className="mb-4">


    <div className="row">



        <div className="col-md-5 text-center">
            <h2 className="h1-responsive font-weight-bold text-center my-4">Contact </h2>
    <p className="text-center w-responsive mx-auto mb-5">Looking forward to hearing from you.</p>
            <ul className="list-unstyled mb-0">
                <li><i className="fas fa-map-marker-alt fa-2x"></i>
                    <p>Arambag, Mirpur, Dhaka.</p>
                </li>

                <li><i className="fas fa-phone mt-4 fa-2x"></i>
                    <p> +880173-0968866 </p>
                </li>

                <li><i className="fas fa-envelope mt-4 fa-2x"></i>
                    <p>sabrina.y.setu@gmail.com</p>
                </li>
            </ul>
        </div>
        
        <div className="col-md-7 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form"  method="POST">


                <div className="row ">


                    <div className="col-md-5">
                        <div className="md-form mb-0">
                                                    <label htmlFor="name" className="">Your name</label>
                            <input type="Text" id="name" name="name" className="form-control" />

                        </div>
                    </div>


                    <div className="col-md-5">
                        <div className="md-form mb-0">
                                                    <label htmlFor="email" className="">Your email</label>
                            <input type="Text" id="email" name="email" className="form-control" />

                        </div>
                    </div>


                </div>

                <div className="row">
                    <div className="col-md-10">
                        <div className="md-form mb-0">
                                                    <label htmlFor="subject" className="">Subject</label>
                            <input type="Text" id="subject" name="subject" className="form-control" />

                        </div>
                    </div>
                </div>

                <div className="row">


                    <div className="col-md-10">

                        <div className="md-form">
                                                    <label htmlFor="message">Your message</label>
                            <textarea type="Text" id="message" name="message" rows="2" className="form-control md-textarea"></textarea>

                        </div>

                    </div>
                </div>

            </form>

            {/* <div className="bg-white p-4 rounded shadow-sm h-100">
                <button class="btn btn-success btn-circle btn-circle-xl m-1">Submit</button>
            </div> */}
           
        </div>

    </div>

</section>
  );
};
export default Contacts;
