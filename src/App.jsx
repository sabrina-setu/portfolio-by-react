import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Home from "./pages/Home";
import NavBar from "./components/NavBar";
import Resume from "./pages/Resume";
import Projects from "./pages/Projects";
import Contacts from "./pages/Contacts";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Home" component={Home} />
          <Route exact path="/Resume" component={Resume} />
          <Route exact path="/Projects" component={Projects} />
          <Route exact path="/Contacts" component={Contacts} />
        </Switch>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
