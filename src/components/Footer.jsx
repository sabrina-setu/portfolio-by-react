const Footer = () => {
  return (
    <footer className="bg-light text-center text-lg-start footer">
      <div className="container p-4">
        <div className="row">
          <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5>Phone</h5>

            <ul className="list-unstyled mb-0">
              <li>+880173-0968866</li>
            </ul>
          </div>

          <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5>Email</h5>

            <ul className="list-unstyled">
              <li>sabrina.y.setu@gmail.com</li>
            </ul>
          </div>

          <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5>Follow Me</h5>
            <div>
              <section>
                <a
                  className="btn btn-primary btn-floating rounded-circle m-1"
                  style={{ backgroundColor: "#3b5998" }}
                  href="https://www.facebook.com/sabrinayeasmin.setu/"
                  role="button"
                >
                  <i className="fab fa-facebook-f"></i>
                </a>
                <a
                  className="btn btn-primary btn-floating rounded-circle m-1"
                  style={{ backgroundColor: "#0082ca" }}
                  href="https://www.linkedin.com/in/sabrina-yeasmin-6a5731155"
                  role="button"
                >
                  <i className="fab fa-linkedin-in"></i>
                </a>

                <a
                  className="btn btn-primary btn-floating rounded-circle m-1"
                  style={{ backgroundColor: "#333333" }}
                  href="https://gitlab.com/sabrina-setu"
                  role="button"
                >
                  <i className="fab fa-gitlab"></i>
                </a>
              </section>
            </div>
          </div>

          <div className="col-lg-3 col-md-6 mb-4 mb-md-0">
            <text className="list-unstyled">© 2021 By Sabrina Yeasmin.</text>
          </div>
        </div>
      </div>
    </footer>
  );
};
export default Footer;
