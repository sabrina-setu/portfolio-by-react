import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to={"/Home"}>
            Sabrina Yeasmin
          </Link>
          <div className="nav justify-content-end">
            <Link
              className="nav-link active"
              aria-current="page"
              to={"/Resume"}
            >
              Resume
            </Link>
            <Link className="nav-link" to={"/Projects"}>
            Projects
            </Link>
            <Link className="nav-link" to={"/Contacts"}>
            Contacts
            </Link>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default NavBar;
